/**
 * Carousel
 * https://flickity.metafizzy.co/
 */

import $ from 'jquery'
import 'slick-carousel'
import 'slick-carousel/slick/slick.scss';

export default function carousel() {

  const carousel = document.querySelector('[data-js="carousel"]');

  // When Carousel exists
  if (carousel) {

    $('.swiper-wrapper').slick({
      infinite: true,
      slidesToShow: 3,
      centerPadding: '20px',
      slidesToScroll: 1,
      prevArrow: $('.swiper-button-prev'),
      nextArrow: $('.swiper-button-next'),
      responsive: [
        {
          breakpoint: 1200,
          settings: {
            slidesToShow: 2,
          }
        },
        {
          breakpoint: 768,
          settings: {
            slidesToShow: 1,
          }
        }
      ]
    });
  }
}
